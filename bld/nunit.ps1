$nunitExe = ".\src\packages\NUnit.Runners.2.6.4\tools\nunit-console.exe"
$testDll = ".\src\RobotCleaner.App.Test\bin\Release\RobotCleaner.App.Test.dll"

New-Item "./src/TestResults" -Type directory -Force

Write-Host ""
Write-Host "Running automated unit tests..." -ForegroundColor "Cyan"
& $nunitExe /framework:net-4.0 /nologo /noshadow /labels /exclude:"Integration" /out:"./src/TestResults/UnitTestResults.txt" /xml:"./src/TestResults/UnitTestResults.xml" $testDll
if($LastExitCode -ne 0) {
	Write-Error "Test failures detected!"
}
Write-Host "Automated unit tests complete!" -ForegroundColor "DarkGreen"

Write-Host "Running integration tests..." -ForegroundColor "Cyan"
& $nunitExe /framework:net-4.0 /nologo /noshadow /labels /include:"Integration" /out:"./src/TestResults/IntegrationTestResults.txt" /xml:"./src/TestResults/IntegrationTestResults.xml" $testDll
if($LastExitCode -ne 0) {
	Write-Error "Test failures detected!"
}
Write-Host "Integration tests complete!" -ForegroundColor "DarkGreen"

Write-Host "Test results output to: .\src\TestResults"
