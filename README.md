#Robot Cleaner
A small application to simulate a cleaning robot used by Cint to tidy their office :-)

                                  _____
                                 |     |
                                 | | | |
                                 |_____|
                           ____ ___|_|___ ____
                          ()___)         ()___)
                          // /|           |\ \\
                         // / |           | \ \\
                        (___) |___________| (___)
                        (___)   (_______)   (___)
                        (___)     (___)     (___)
                        (___)      |_|      (___)
                        (___)  ___/___\___   | |
                         | |  |           |  | |
                         | |  |___________| /___\
                        /___\  |||     ||| //   \\
                       //   \\ |||     ||| \\   //
                       \\   // |||     |||  \\ //
                        \\ // ()__)   (__()
                              ///       \\\
                             ///         \\\
                           _///___     ___\\\_
                          |_______|   |_______|


## Robot cleaning technique
1. Create office board
2. Clean start position
3. Go through robot's moves:
    - Add the absolute value of the magnitude to the total
    - Compute the movement lines as two points on the grid
4. Examine each of the movement lines against each other:
    - Check if they intersect
    - Store the overlapping points between the two lines
5. Return total of the clean vertices and subtract the number of unique overlapping points

## Build requirements

  - .NET 4.5
  - Visual Studio 2013
  - MSBuild
  
## Development dependancy requirements

  - Ninject
  - NUnit
  - Moq
  
## Usage

Running the `.\build.ps1` command will:

  - Install dependancies
  - Build application
  - Run all unit tests
  - Run all integration tests
  - Output test results to `.\src\TestResults`

