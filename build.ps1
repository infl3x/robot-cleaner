$ErrorActionPreference = "Stop"

Write-Host ""
Write-Host "------------------------------------------------"
Write-Host "Building..." -ForegroundColor "DarkGreen"
Write-Host "------------------------------------------------"
Write-Host ""

# Dependancies
& .\bld\nuget.ps1

# Build
& .\bld\msbuild.ps1

# Test
& .\bld\nunit.ps1

Write-Host ""
Write-Host "------------------------------------------------"
Write-Host "Build complete!"  -ForegroundColor "DarkGreen"
Write-Host "------------------------------------------------"


if($LastExitCode -ne 0) {
	exit 1
} else {
	exit 0
}
