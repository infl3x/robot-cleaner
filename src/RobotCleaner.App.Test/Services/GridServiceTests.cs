﻿using NUnit.Framework;
using RobotCleaner.App.Services;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test.Services
{
    [TestFixture]
    public class GridServiceTests
    {

        private IGridService _gridService;

        [SetUp]
        public void SetUp()
        {
            _gridService = new GridService();
        }

        #region Touching Start/Finish points

        [Test]
        public void ShouldDetectIntersectionWhenStartingOnStartOfAnotherLine()
        {
            // This style of movement:
            //
            //   2 |                   
            //     |                   
            //   1 |                  x
            //     |                  |
            //   0 |                  |
            //     |                  | 
            //  -1 |      -----------x|
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(1, 1),
                Finish = new Node(1, -1)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -1),
                Finish = new Node(-1, -1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(1, -1) });
        }

        [Test]
        public void ShouldDetectIntersectionWhenStartingOnFinishOfAnotherLine()
        {
            // This style of movement:
            //
            //   2 |                   
            //     |                   
            //   1 |                  |
            //     |                  |
            //   0 |                  |
            //     |                  | 
            //  -1 |      ------------x
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Finish = new Node(1, -1),
                Start = new Node(1, 1)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -1),
                Finish = new Node(-1, -1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(1, -1) });
        }

        [Test]
        public void ShouldDetectIntersectionWhenFinishingOnStartOfAnotherLine()
        {
            // This style of movement:
            //
            //   2 |                   
            //     |                   
            //   1 |      x           
            //     |      |           
            //   0 |      |           
            //     |      |            
            //  -1 |      |x----------x
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-1, 1),
                Finish = new Node(-1, -1)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -1),
                Finish = new Node(-1, -1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(-1, -1) });
        }

        [Test]
        public void ShouldDetectIntersectionWhenFinishingOnFinishOfAnotherLine()
        {
            // This style of movement:
            //
            //   2 |                   
            //     |                   
            //   1 |      |
            //     |      |
            //   0 |      |
            //     |      | 
            //  -1 |      x-----------x
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-1, -1),
                Finish = new Node(-1, 1)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -1),
                Finish = new Node(-1, -1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(-1, -1) });
        }

        #endregion

        #region Intersection

        [Test]
        public void ShouldDetectFullIntersection()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            |
            //     |            |
            //   0 |x-------------------------
            //     |            |
            //  -1 |            |
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(2, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(0, 0) });
        }

        [Test]
        public void ShouldDetectTerminousIntersectionOnXAxis()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            |
            //     |            |
            //   0 |x-----------x-------------
            //     |            
            //  -1 |            
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(2, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(0, 0) });
        }

        [Test]
        public void ShouldDetectTerminousIntersectionOnYAxis()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            |
            //     |            |
            //   0 |-----------x|
            //     |            |
            //  -1 |            |
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 2)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(-2, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(0, 0) });
        }

        [Test]
        public void ShouldNotDetectIntersectionWhenLinesDoNotCross()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            x
            //     |            
            //   0 |x-----------x-------------
            //     |            
            //  -1 |            
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(2, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 1),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.None);
        }

        [Test]
        public void ShouldNotDetectIntersectionWhenLineDoesNotTouchLineOnXAxis()
        {
            // This style of movement:
            //
            //   2 |            
            //     |            
            //   1 |      x-------------------
            //     |            
            //   0 |                  |
            //     |                  |
            //  -1 |                  x
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-1, 1),
                Finish = new Node(2, 1)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -1),
                Finish = new Node(1, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.None);
        }

        [Test]
        public void ShouldNotDetectIntersectionWhenLineDoesNotTouchLineOnYAxis()
        {
            // This style of movement:
            //
            //   2 |            
            //     |            
            //   1 |      |
            //     |      |
            //   0 |      |     -------------x
            //     |      |
            //  -1 |      x
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-1, -1),
                Finish = new Node(-1, 1)
            };
            Line line2 = new Line()
            {
                Start = new Node(2, 0),
                Finish = new Node(0, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.None);
        }

        [Test]
        public void ShouldDetectIntersectionWhen98304()
        {
            // This style of movement:
            //     
            //   98,304 |x---------------|
            //          |                |
            //        0 |                |
            //          |                |
            //  -98,304 |________________x
            //  -98,304       0       98,304

            Line line1 = new Line()
            {
                Start = new Node(-98304, 98304),
                Finish = new Node(98304, 98304)
            };
            Line line2 = new Line()
            {
                Start = new Node(98304, 98304),
                Finish = new Node(98304, -98304)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Intersection, new Node[] { new Node(98304, 98304) });
        }

        #endregion

        #region Parallel

        [Test]
        public void ShouldDetectParallelOnXAxis()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |x-------------------------
            //     |    
            //   0 |x-------------------------
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(2, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(-2, 1),
                Finish = new Node(2, 1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Parallel);
        }

        [Test]
        public void ShouldDetectParallelOnYAxis()
        {
            // This style of movement:
            //
            //   2 |            |     |
            //     |            |     |
            //   1 |            |     |
            //     |            |     |
            //   0 |            |     |
            //     |            |     |
            //  -1 |            |     |
            //     |            |     |
            //  -2 |____________x_____x_______
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 2)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, -2),
                Finish = new Node(1, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Parallel);
        }

        #endregion

        #region Collinear

        #region With Overlap

        #region Total

        [Test]
        public void ShouldDetectCollinearWithTotalOverlapOnXAxisWhenBothNegative()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x-----------x------x
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -4     -3    -2     -1     0

            Line line1 = new Line()
            {
                Start = new Node(-4, 0),
                Finish = new Node(0, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(-3, 0),
                Finish = new Node(-1, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(-3, 0),
                new Node(-2, 0),
                new Node(-1, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithTotalOverlapOnXAxisWhenBothPositive()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x-----------x------x
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    0      1     2      3      4

            Line line1 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(4, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, 0),
                Finish = new Node(3, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(1, 0),
                new Node(2, 0),
                new Node(3, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearaWithTotalOverlapOnXAxisWhenMixedSigns()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x-----------x------x
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(2, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(-1, 0),
                Finish = new Node(1, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(-1, 0),
                new Node(0, 0),
                new Node(1, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearaWithTotalOverlapOnYAxisWhenMixedSigns()
        {
            // This style of movement:
            // 
            //   2 |            x
            //     |            |
            //   1 |            x
            //     |            | 
            //   0 |            |
            //     |            |
            //  -1 |            x
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 2)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, -1),
                Finish = new Node(0, 1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, -1),
                new Node(0, 0),
                new Node(0, 1)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithTotalOverlapOnYAxisWhenBothNegative()
        {
            // This style of movement:
            // 
            //   0 |            x
            //     |            |
            //  -1 |            x
            //     |            | 
            //  -2 |            |
            //     |            |
            //  -3 |            x
            //     |            |
            //  -4 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -4),
                Finish = new Node(0, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, -3),
                Finish = new Node(0, -1)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, -3),
                new Node(0, -2),
                new Node(0, -1)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithTotalOverlapOnYAxisWhenBothPositive()
        {
            // This style of movement:
            // 
            //   4 |            x
            //     |            |
            //   3 |            x
            //     |            | 
            //   2 |            |
            //     |            |
            //   1 |            x
            //     |            |
            //   0 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(0, 4)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 1),
                Finish = new Node(0, 3)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, 1),
                new Node(0, 2),
                new Node(0, 3)
            });
        }

        #endregion

        #region Patial

        [Test]
        public void ShouldDetectCollinearWithOverlapOnXAxisWhenMixedSigns()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x----------->------>
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(1, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(-1, 0),
                Finish = new Node(2, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(-1, 0),
                new Node(0, 0),
                new Node(1, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithOverlapOnXAxisWhenBothNegative()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x----------->------>
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -4     -3    -2     -1     0

            Line line1 = new Line()
            {
                Start = new Node(-4, 0),
                Finish = new Node(-1, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(-3, 0),
                Finish = new Node(0, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(-3, 0),
                new Node(-2, 0),
                new Node(-1, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithOverlapOnXAxisWhenBothPositive()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----x----------->------>
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    0      1     2      3      4

            Line line1 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(3, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(1, 0),
                Finish = new Node(4, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(1, 0),
                new Node(2, 0),
                new Node(3, 0)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithOverlapOnYAxisWhenMixedSigns()
        {
            // This style of movement:
            // 
            //   2 |            ^
            //     |            |
            //   1 |            ^
            //     |            | 
            //   0 |            |
            //     |            |
            //  -1 |            x
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 1)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, -1),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, -1),
                new Node(0, 0),
                new Node(0, 1)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithOverlapOnYAxisWhenBothNegative()
        {
            // This style of movement:
            // 
            //   0 |            ^
            //     |            |
            //  -1 |            ^
            //     |            | 
            //  -2 |            |
            //     |            |
            //  -3 |            x
            //     |            |
            //  -4 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -4),
                Finish = new Node(0, -1)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, -3),
                Finish = new Node(0, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, -3),
                new Node(0, -2),
                new Node(0, -1)
            });
        }

        [Test]
        public void ShouldDetectCollinearWithOverlapOnYAxisWhenBothPositive()
        {
            // This style of movement:
            // 
            //   4 |            ^
            //     |            |
            //   3 |            ^
            //     |            | 
            //   2 |            |
            //     |            |
            //   1 |            x
            //     |            |
            //   0 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(0, 3)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 1),
                Finish = new Node(0, 4)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] {
                new Node(0, 1),
                new Node(0, 2),
                new Node(0, 3)
            });
        }

        #endregion

        #endregion

        #region Without Overlap

        [Test]
        public void ShouldDetectCollinearWithNoOverlapOnXAxis()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----      x-------------
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(-1, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(2, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear);
        }

        [Test]
        public void ShouldDetectCollinearWithNoOverlapOnYAxis()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            |
            //     |            |
            //   0 |            x
            //     |    
            //  -1 |            |
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, -1)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear);
        }

        [Test]
        public void ShouldDetectCollinearTouchingOnXAxis()
        {
            // This style of movement:
            //
            //   2 |    
            //     |    
            //   1 |
            //     |    
            //   0 |x-----------x-------------
            //     |    
            //  -1 |    
            //     |    
            //  -2 |__________________________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(-2, 0),
                Finish = new Node(0, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(2, 0)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] { new Node(0, 0) });
        }

        [Test]
        public void ShouldDetectCollinearTouchingOnYAxis()
        {
            // This style of movement:
            //
            //   2 |            |
            //     |            |
            //   1 |            |
            //     |            |
            //   0 |            x
            //     |            |
            //  -1 |            |
            //     |            |
            //  -2 |____________x_____________
            //    -2     -1     0     1      2

            Line line1 = new Line()
            {
                Start = new Node(0, -2),
                Finish = new Node(0, 0)
            };
            Line line2 = new Line()
            {
                Start = new Node(0, 0),
                Finish = new Node(0, 2)
            };

            AssertGridLineRelationship(line1, line2, LineRelationship.Collinear, new Node[] { new Node(0, 0) });
        }

        #endregion

        #endregion

        private void AssertGridLineRelationship(Line line1, Line line2, LineRelationship lineRelationship, params Node[] overlappingCoordinates)
        {
            GridLineRelationship gridLineRelationship = _gridService.DoLineSegmentsIntersect(line1, line2);
            Assert.That(gridLineRelationship.LineRelationship, Is.EqualTo(lineRelationship));

            if (lineRelationship == LineRelationship.Intersection || lineRelationship == LineRelationship.Collinear)
            {
                overlappingCoordinates = overlappingCoordinates ?? new Node[] { };

                Assert.That(gridLineRelationship.OverlappingCoordinates.Length, Is.EqualTo(overlappingCoordinates.Length));
                foreach (Node overlappingCoordinate in gridLineRelationship.OverlappingCoordinates)
                {
                    Assert.That(overlappingCoordinates.Any(c => c.X == overlappingCoordinate.X && c.Y == overlappingCoordinate.Y));
                }
            }
        }

    }
}
