﻿using NUnit.Framework;
using RobotCleaner.App.Infrastructure;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test
{
    [TestFixture]
    public class OfficeTests
    {

        private Office _office;

        [SetUp]
        public void SetUp()
        {
            _office = new Office();
        }

        [Test]
        public void ShouldBuildGridWithOddSize()
        {
            Grid grid = _office.Initialise(new CleanerConfiguration() { OfficeLength = 3, OfficeWidth = 3 });
            Assert.That(grid.LowerXBound, Is.EqualTo(-1));
            Assert.That(grid.UpperXBound, Is.EqualTo(1));
            Assert.That(grid.LowerYBound, Is.EqualTo(-1));
            Assert.That(grid.UpperYBound, Is.EqualTo(1));
        }

        [Test]
        public void ShouldCleanGridNode()
        {
            Grid grid = _office.Initialise(new CleanerConfiguration() { OfficeLength = 3, OfficeWidth = 3 });
            for (int x = grid.LowerXBound; x < grid.UpperXBound; x++)
            {
                for (int y = grid.LowerYBound; y < grid.UpperYBound; y++)
                {
                    Assert.DoesNotThrow(() => _office.Clean(x, y));
                }
            }
        }

        [Test]
        public void ShouldThrowExceptionOnGridWithEvenWidth()
        {
            Assert.Throws<InvalidGridException>(() => _office.Initialise(new CleanerConfiguration() { OfficeLength = 1, OfficeWidth = 2 }));
        }

        [Test]
        public void ShouldThrowExceptionOnGridWithEvenHeight()
        {
            Assert.Throws<InvalidGridException>(() => _office.Initialise(new CleanerConfiguration() { OfficeLength = 2, OfficeWidth = 1 }));
        }

    }
}
