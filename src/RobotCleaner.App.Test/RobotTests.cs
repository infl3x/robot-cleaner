﻿using Moq;
using NUnit.Framework;
using RobotCleaner.App.Infrastructure;
using RobotCleaner.App.Model;
using RobotCleaner.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test
{
    [TestFixture]
    public class RobotTests
    {
        private Mock<IGridService> _gridService;
        private Mock<IOffice> _office;

        private CleanerConfiguration _cleanerConfiguration;
        private Robot _robot;

        [SetUp]
        public void SetUp()
        {
            _gridService = new Mock<IGridService>();
            _office = new Mock<IOffice>();

            _cleanerConfiguration = new CleanerConfiguration()
            {
                OfficeWidth = 5,
                OfficeLength = 5,
                StartingCoordinate = new int[] { 0, 0 }
            };

            _robot = new Robot(_gridService.Object);
        }

        [Test]
        public void ShouldRemoveCollinearMovementLineIntersections()
        {
            _cleanerConfiguration.Moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 10 },
                new Vector() { Direction = Direction.S, Magnitude = 10 }
            };
            _gridService.Setup(g => g.DoLineSegmentsIntersect(It.IsAny<Line>(), It.IsAny<Line>())).Returns(new GridLineRelationship()
            {
                LineRelationship = LineRelationship.Collinear,
                OverlappingCoordinates = GetFakeGridLineRelationshipFor(numberOfOverlappingVertices: 10)
            });

            _robot.Initialise(_cleanerConfiguration.StartingCoordinate, _cleanerConfiguration.Moves);

            Assert.That(_robot.Clean(_office.Object), Is.EqualTo(11));
        }

        [Test]
        public void ShouldRemoveIntersectingMovementLineIntersections()
        {
            _cleanerConfiguration.Moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 10 },
                new Vector() { Direction = Direction.W, Magnitude = 10 }
            };
            _gridService.Setup(g => g.DoLineSegmentsIntersect(It.IsAny<Line>(), It.IsAny<Line>())).Returns(new GridLineRelationship()
            {
                LineRelationship = LineRelationship.Intersection,
                OverlappingCoordinates = GetFakeGridLineRelationshipFor(numberOfOverlappingVertices: 1)
            });

            _robot.Initialise(_cleanerConfiguration.StartingCoordinate, _cleanerConfiguration.Moves);

            Assert.That(_robot.Clean(_office.Object), Is.EqualTo(20));
        }

        [Test]
        public void ShouldNotRemoveParallelMovementLines()
        {
            _cleanerConfiguration.Moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 10 },
                new Vector() { Direction = Direction.S, Magnitude = 10 }
            };
            _gridService.Setup(g => g.DoLineSegmentsIntersect(It.IsAny<Line>(), It.IsAny<Line>())).Returns(new GridLineRelationship()
            {
                LineRelationship = LineRelationship.Parallel,
                //Overlap = 0
            });

            _robot.Initialise(_cleanerConfiguration.StartingCoordinate, _cleanerConfiguration.Moves);

            Assert.That(_robot.Clean(_office.Object), Is.EqualTo(21));
        }

        [Test]
        public void ShouldNotRemoveMovementLinesWhenTheyDoNotIntersect()
        {
            _cleanerConfiguration.Moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 10 },
                new Vector() { Direction = Direction.W, Magnitude = 10 }
            };
            _gridService.Setup(g => g.DoLineSegmentsIntersect(It.IsAny<Line>(), It.IsAny<Line>())).Returns(new GridLineRelationship()
            {
                LineRelationship = LineRelationship.None,
                //Overlap = 0
            });

            _robot.Initialise(_cleanerConfiguration.StartingCoordinate, _cleanerConfiguration.Moves);
            
            Assert.That(_robot.Clean(_office.Object), Is.EqualTo(21));
        }

        [Test]
        [Ignore("Unsed for debugging integration tests")]
        public void Debug()
        {
            // This style of movement:
            // 
            //   2 ||-------------------------     
            //     ||                        |
            //   1 ||     |------------      |
            //     ||     |           |      |
            //   0 ||     |           |      |
            //     ||     |           |      |
            //  -1 ||     |     x-----|      |
            //     ||     |                  |
            //  -2 |x_____|__________________|
            //    -2     -1     0     1      2

            _cleanerConfiguration.StartingCoordinate = new int[] { -100000, -100000 };
            _cleanerConfiguration.OfficeWidth = _cleanerConfiguration.OfficeLength = 200001;
            int magnitude = 200000;
            int totalCleaned = 1;
            for (int i = 0; i < 3000; i++) // (10000 / 4), x = 1697 is the bad one here
            {
                _cleanerConfiguration.Moves.Add(new Vector() { Direction = Direction.N, Magnitude = magnitude });
                totalCleaned += magnitude;

                if (i > 0)
                    magnitude--;
                _cleanerConfiguration.Moves.Add(new Vector() { Direction = Direction.E, Magnitude = magnitude });
                totalCleaned += magnitude;

                _cleanerConfiguration.Moves.Add(new Vector() { Direction = Direction.S, Magnitude = magnitude });
                totalCleaned += magnitude;

                magnitude--;
                _cleanerConfiguration.Moves.Add(new Vector() { Direction = Direction.W, Magnitude = magnitude });
                totalCleaned += magnitude;
            }

            Office office = new Office();
            office.Initialise(_cleanerConfiguration);

            _robot = new Robot(new GridService());
            _robot.Initialise(_cleanerConfiguration.StartingCoordinate, _cleanerConfiguration.Moves);

            Assert.That(_robot.Clean(office), Is.EqualTo(totalCleaned));
        }

        private static Node[] GetFakeGridLineRelationshipFor(int numberOfOverlappingVertices)
        {
            List<Node> overlappingVertices = new List<Node>();
            for (int i = 0; i < numberOfOverlappingVertices; i++)
            {
                overlappingVertices.Add(new Node(10 + i, 200));
            }

            return overlappingVertices.ToArray();
        }

    }
}
