﻿using Moq;
using NUnit.Framework;
using RobotCleaner.App.Configuration;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test.Configuration
{
    [TestFixture]
    public class MovesReaderTests
    {

        private readonly Mock<IInputReader> _inputReader = new Mock<IInputReader>();
        private MovesReader _movesReader;

        [SetUp]
        public void SetUp()
        {
            _movesReader = new MovesReader(_inputReader.Object);
        }

        [Test]
        public void ShouldReadSingleMove()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("N 2");

            List<Vector> moves = _movesReader.Read(1);

            Assert.That(moves.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReadMultipleMoves()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("N 2");

            List<Vector> moves = _movesReader.Read(3);

            Assert.That(moves.Count, Is.EqualTo(3));
        }

        [Test]
        public void ShouldReadAllDirectionTypes()
        {
            List<Direction> allDirections = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToList();
            allDirections.ForEach(direction =>
            {
                _inputReader.Setup(i => i.ReadLine()).Returns(direction.ToString().First() + " 3");

                List<Vector> moves = _movesReader.Read(1);

                Assert.That(moves.Count, Is.EqualTo(1));
                Assert.That(moves.First().Direction, Is.EqualTo(direction));
            });
        }

        [Test]
        public void ShouldThrowExceptionWhenInvalidDirectionEntered()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("L 3");

            Assert.Throws<ArgumentException>(() => _movesReader.Read(1));
        }

        [Test]
        public void ShouldThrowExceptionWhenInvalidMagnitudeEntered()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("W P");

            Assert.Throws<FormatException>(() => _movesReader.Read(1));
        }
    }
}
