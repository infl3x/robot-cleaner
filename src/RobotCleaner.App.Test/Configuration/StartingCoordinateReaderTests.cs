﻿using Moq;
using NUnit.Framework;
using RobotCleaner.App.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test.Configuration
{
    [TestFixture]
    public class StartingCoordinateReaderTests
    {
        private readonly Mock<IInputReader> _inputReader = new Mock<IInputReader>();
        private StartingCoordinateReader _startingCoordinateReader;

        [SetUp]
        public void SetUp()
        {
            _startingCoordinateReader = new StartingCoordinateReader(_inputReader.Object);
        }

        [Test]
        public void ShouldReadValidStartingCoordinate()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("1 2");

            Assert.That(_startingCoordinateReader.Read(), Is.EqualTo(new[] { 1, 2 }));
        }

        [Test]
        public void ShouldThrowExceptionWhenInvalidStartingCoordinateEntered()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("s %");

            Assert.Throws<FormatException>(() => _startingCoordinateReader.Read());
        }
    }
}
