﻿using Moq;
using NUnit.Framework;
using RobotCleaner.App.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test.Configuration
{
    [TestFixture]
    public class NumberOfCommandsReaderTests
    {

        private readonly Mock<IInputReader> _inputReader = new Mock<IInputReader>();

        [Test]
        public void ShouldReadValidNumberOfCommands()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("1");

            NumberOfCommandsReader configurationReader = new NumberOfCommandsReader(_inputReader.Object);
            
            Assert.That(configurationReader.Read(), Is.EqualTo(1));
        }

        [Test]
        public void ShouldThrowExceptionWhenInvalidNumberOfCommandsEntered()
        {
            _inputReader.Setup(i => i.ReadLine()).Returns("s");

            NumberOfCommandsReader configurationReader = new NumberOfCommandsReader(_inputReader.Object);

            Assert.Throws<FormatException>(() => configurationReader.Read());
        }
    }
}
