﻿using Moq;
using NUnit.Framework;
using RobotCleaner.App.Configuration;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test.Configuration
{
    [TestFixture]
    public class CleanerConfigurationReaderTests
    {

        private Mock<INumberOfCommandsReader> _numberOfCommandsReader;
        private Mock<IStartingCoordinateReader> _startingCoordinateReader;
        private Mock<IMovesReader> _movesReader;
        private CleanerConfigurationReader _configurationReader;

        [SetUp]
        public void SetUp()
        {
            _numberOfCommandsReader = new Mock<INumberOfCommandsReader>();
            _startingCoordinateReader = new Mock<IStartingCoordinateReader>();
            _movesReader = new Mock<IMovesReader>();
            _configurationReader = new CleanerConfigurationReader(_numberOfCommandsReader.Object, _startingCoordinateReader.Object, _movesReader.Object);
        }

        [Test]
        public void ShouldReadNumberOfCommands()
        {
            _numberOfCommandsReader.Setup(n => n.Read()).Returns(3);
            _movesReader.Setup(n => n.Read(It.IsAny<int>())).Returns(new List<Vector>() { new Vector(), new Vector(), new Vector() });

            Assert.That(_configurationReader.Read().Moves.Count, Is.EqualTo(3));
        }

        [Test]
        public void ShouldReadStartingCoordinate()
        {
            _startingCoordinateReader.Setup(n => n.Read()).Returns(new int[] { 1, 2 });

            Assert.That(_configurationReader.Read().StartingCoordinate, Is.EqualTo(new int[] { 1, 2 }));
        }

        [Test]
        public void ShouldReadMoves()
        {
            _movesReader.Setup(n => n.Read(It.IsAny<int>())).Returns(new List<Vector>() { new Vector() });

            Assert.That(_configurationReader.Read().Moves.Count, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReadOfficeWidth()
        {
            Assert.That(_configurationReader.Read().OfficeWidth, Is.EqualTo(123));
        }

        [Test]
        public void ShouldReadOfficeLength()
        {
            Assert.That(_configurationReader.Read().OfficeLength, Is.EqualTo(321));
        }

    }
}
