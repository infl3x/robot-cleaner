﻿using NUnit.Framework;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RobotCleaner.App.Test
{
    [TestFixture]
    [Category("Integration")]
    public class IntegrationTests
    {

        // Output format: => Cleaned: xxx
        private readonly Regex _outputRegex = new Regex("(?<cleaned>[0-9]+)", RegexOptions.Compiled);

        [Test]
        public void ShouldCleanAssignmentExampleOffice()
        {
            // This style of movement:
            //
            //  24 |
            //     |
            //  23 |                         x
            //     |                         |
            //  22 |            x-------------
            //     |            
            //  21 |            
            //     |            
            //  20 |___________________________
            //     8     9     10     11     12

            int[] startingCoordinate = new int[] { 10, 22 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.N, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(4));
        }

        [Test]
        public void ShouldProcessEmptyMovementInstruction()
        {
            // This style of movement:
            //
            //   2 |
            //     |
            //   1 |
            //     |
            //   0 |            x
            //     |            
            //  -1 |            
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 0 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(1));
        }

        [Test]
        public void ShouldCleanVerticesInEveryQuadrant()
        {
            // This style of movement:
            //
            //   2 ||-------------------------
            //     ||                         |
            //   1 ||                         |
            //     ||                         |
            //   0 ||                         |
            //     ||                         |
            //  -1 ||                         |
            //     ||_________________________|
            //  -2 |x_________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -2, -2 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 4 },
                new Vector() { Direction = Direction.E, Magnitude = 4 },
                new Vector() { Direction = Direction.S, Magnitude = 4 },
                new Vector() { Direction = Direction.W, Magnitude = 4 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(16));
        }

        [Test]
        public void ShouldCleanStartingPositionOnly()
        {
            // This style of movement:
            //
            //   2 |
            //     |
            //   1 |
            //     |
            //   0 |            x
            //     |            
            //  -1 |            
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>();

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(1));
        }

        [Test]
        public void ShouldNotCleanSameVertexTwice()
        {
            // This style of movement:
            //
            //   2 |
            //     |
            //   1 |            ˅
            //     |            |
            //   0 |            x
            //     |            
            //  -1 |            
            //     |            
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.S, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(2));
        }

        [Test]
        public void ShouldCleanAllVerticesInSquare()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      |-----------
            //     |      |          |
            //   0 |      |    x     |
            //     |      |    |     |
            //  -1 |      x     -----|
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(9));
        }

        [Test]
        public void ShouldCleanInSquare()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      |-----------
            //     |      |          |
            //   0 |      |          |
            //     |      |          |
            //  -1 |      x ---------|
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.W, Magnitude = 2 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldCleanPerimetre()
        {
            // This style of movement:
            //
            // 100,000 |---------------------------------------x
            //         ||                                      |
            //  50,000 ||                                      |
            //         ||                                      |
            //       0 ||                                      |
            //         ||                                      |
            // -50,000 ||                                      |
            //         ||______________________________________|
            // -100,000|________________________________________
            //      -100,000    -50,000    0    50,000   100,000

            int[] startingCoordinate = new int[] { 100000, 100000 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.S, Magnitude = 200000 },
                new Vector() { Direction = Direction.W, Magnitude = 200000 },
                new Vector() { Direction = Direction.N, Magnitude = 200000 },
                new Vector() { Direction = Direction.E, Magnitude = 200000 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(4 * 200000));
        }

        #region Complex Movement

        [Test]
        public void ShouldCleanWithComplexMovements1()
        {
            // This style of movement:
            //
            //   2 |      -------------x
            //     |      |            |
            //   1 |      +-------------------
            //     |      |            |     |
            //   0 |      |    --------------|
            //     |      |    |       | 
            //  -1 |      x    |-------
            //     |      |
            //  -2 |______x___________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 3 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 3 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 4 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(15));
        }

        [Test]
        public void ShouldCleanWithComplexMovements2()
        {
            // This style of movement:
            //
            //   2 |      -------|     -------|
            //     |      |      |     |      |
            //   1 |      |      ------|------|
            //     |      |            |      |
            //   0 |      |            |------|
            //     |      |                   |
            //  -1 |x-----x--------------------
            //     |            
            //  -2 |___________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -2, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 4 },
                new Vector() { Direction = Direction.N, Magnitude = 3 },
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.S, Magnitude = 3 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(16));
        }

        [Test]
        public void ShouldCleanWithComplexMovements3()
        {
            // This style of movement:
            //
            //   2 |      
            //     |      
            //   1 |      
            //     |      
            //   0 |      ------------|
            //     |      |           |
            //  -1 |x-----|------------
            //     |            |
            //  -2 |____________x______________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -2, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 3 },
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.S, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldCleanWithComplexMovements4()
        {
            // This style of movement:
            //
            //   2 |            |--------------
            //     |            |             |
            //   1 |            x-------------x
            //     |                  |       |
            //   0 |                  |       |
            //     |                  |       |
            //  -1 |                  --------|
            //     |
            //  -2 |___________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 2 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldCleanTenThousandOfficeMoves()
        {
            // This style of movement:
            // 
            //   2 ||------     |------      x
            //     ||     |     |     |      |
            //   1 ||     |     |     |      |
            //     ||     |     |     |      |
            //   0 ||     |     |     |      |
            //     ||     |     |     |      |
            //  -1 ||     |     |     |      |
            //     ||     |_____|     |______|
            //  -2 |x_________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -100000, -100000 };
            List<Vector> moves = new List<Vector>();

            Direction lastDirection = Direction.S;
            int magnitude = 0;
            for (int i = -100000; i < -90000; i++)
            {
                if (lastDirection == Direction.S)
                {
                    lastDirection = Direction.N;
                    magnitude = 200000;
                }
                else if (lastDirection == Direction.N)
                {
                    lastDirection = Direction.S;
                    magnitude = -200000;
                }

                moves.Add(new Vector() { Direction = lastDirection, Magnitude = magnitude });

                if (i != 100000)
                    moves.Add(new Vector() { Direction = Direction.E, Magnitude = 1 });
            }

            long totalCleaned = (long)10000 * (long)200001 + 1; // +1 because the last movement is 1 space to the east

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(totalCleaned));
        }

        [Test]
        public void ShouldCleanMaximumNumberOfOfficeNodes()
        {
            // This style of movement:
            // 
            //   2 ||-------------------------     
            //     ||                        |
            //   1 ||     |------------      |
            //     ||     |           |      |
            //   0 ||     |           |      |
            //     ||     |           |      |
            //  -1 ||     |     x-----|      |
            //     ||     |                  |
            //  -2 |x_____|__________________|
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -100000, -100000 };
            List<Vector> moves = new List<Vector>();

            int magnitude = 200000;
            long totalCleaned = 1;
            for (int i = 0; i < (10000 / 4); i++)
            {
                moves.Add(new Vector() { Direction = Direction.N, Magnitude = magnitude });
                totalCleaned += magnitude;

                if (i > 0)
                    magnitude--;
                moves.Add(new Vector() { Direction = Direction.E, Magnitude = magnitude });
                totalCleaned += magnitude;

                moves.Add(new Vector() { Direction = Direction.S, Magnitude = magnitude });
                totalCleaned += magnitude;

                magnitude--;
                moves.Add(new Vector() { Direction = Direction.W, Magnitude = magnitude });
                totalCleaned += magnitude;
            }

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(totalCleaned));
        }

        #endregion

        #region Intersection

        [Test]
        public void ShouldNotCleanIntersectingVertexTwice()
        {
            // This style of movement:
            //
            //   2 |           |-------------
            //     |           |            |
            //   1 |      x----|------------|
            //     |           |            
            //   0 |           x
            //     |
            //  -1 |    
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 3 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldNotCleanDoubleIntersectingVertexTwice()
        {
            // This style of movement:
            //
            //   2 |                   x
            //     |                   |
            //   1 |      |-------------------
            //     |      |            |     |
            //   0 |      |    --------------|
            //     |      |    |       | 
            //  -1 |      x    |-------
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 3 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 3 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(12));
        }

        #endregion

        #region Collinear

        [Test]
        public void ShouldCleanCollinearOnXAxisFinishingInMiddle()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      |-----------
            //     |      |          |
            //   0 |      |          |
            //     |      |          |
            //  -1 |       -----x----|
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.W, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 2 },
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.W, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldCleanCollinearOnXAxisFinishingInLine()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      
            //     |             
            //   0 |             
            //     |             
            //  -1 |      x----->-----x
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(3));
        }

        [Test]
        public void ShouldCleanCollinearOnXAxisFinishingAtStart()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      
            //     |             
            //   0 |             
            //     |             
            //  -1 |      x-----<
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 1 },
                new Vector() { Direction = Direction.W, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(2));
        }

        [Test]
        public void ShouldCleanCollinearOnYAxisFinishingInMiddle()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      |-----------
            //     |      |          |
            //   0 |      x          |
            //     |      |          |
            //  -1 |       ----------|
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.E, Magnitude = 2 },
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.W, Magnitude = 2 },
                new Vector() { Direction = Direction.N, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(8));
        }

        [Test]
        public void ShouldCleanCollinearOnYAxisFinishingInLine()
        {
            // This style of movement:
            // 
            //   2 |    
            //     |    
            //   1 |      x
            //     |      |      
            //   0 |      ^       
            //     |      |       
            //  -1 |      x
            //     |   
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, -1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 1 },
                new Vector() { Direction = Direction.N, Magnitude = 1 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(3));
        }

        [Test]
        public void ShouldCleanCollinearOnYAxisFinishingAtStart()
        {
            // This style of movement:
            // 
            //   2 |      
            //     |      
            //   1 |      x
            //     |      |      
            //   0 |      |      
            //     |      |      
            //  -1 |      ^
            //     |      
            //  -2 |__________________________
            //    -2     -1     0     1      2

            int[] startingCoordinate = new int[] { -1, 1 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.S, Magnitude = 2 },
                new Vector() { Direction = Direction.N, Magnitude = 2 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(3));
        }

        #endregion

        #region Borders

        [Test]
        public void ShouldCleanEastBorder()
        {
            // This style of movement:
            //
            //   2 |
            //     |
            //   1 |
            //     |
            //   0 |x---------------------------x
            //     |
            //  -1 |
            //     |
            //  -2 |_____________________________
            //     0         50,000       100,000

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.E, Magnitude = 100000 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(100001)); // +1 because starting at 0,0
        }

        [Test]
        public void ShouldCleanWestBorder()
        {
            // This style of movement:
            //
            //   2 |
            //     |
            //   1 |
            //     |
            //   0 |x---------------------------x
            //     |
            //  -1 |
            //     |
            //  -2 |_____________________________
            //   -100,000       -50,000         0

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.W, Magnitude = 100000 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(100001)); // +1 because starting at 0,0
        }

        [Test]
        public void ShouldCleanSouthBorder()
        {
            // This style of movement:
            //
            //        0 |              x
            //          |              |
            //          |              |
            //          |              |
            //  -50,000 |              |
            //          |              |
            //          |              |
            //          |              |
            // -100,000 |______________x_____________
            //           -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.S, Magnitude = 100000 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(100001)); // +1 because starting at 0,0
        }

        [Test]
        public void ShouldCleanNorthBorder()
        {

            // This style of movement:
            //
            // 100,000 |            x
            //         |            |
            //         |            |
            //         |            |
            //  50,000 |            |
            //         |            |
            //         |            |
            //         |            |
            //       0 |____________x_____________
            //        -2     -1     0     1      2

            int[] startingCoordinate = new int[] { 0, 0 };
            List<Vector> moves = new List<Vector>()
            {
                new Vector() { Direction = Direction.N, Magnitude = 100000 }
            };

            Assert.That(RunApplication(startingCoordinate, moves), Is.EqualTo(100001));  // +1 because starting at 0,0
        }

        #endregion

        private int RunApplication(int[] startingCoordinate, List<Vector> moves)
        {
            FileInfo applicationExecutable = GetApplicationExecutable();

            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = applicationExecutable.Directory.FullName;
            process.StartInfo.FileName = applicationExecutable.FullName;
            process.StartInfo.CreateNoWindow = true;
            process.Start();

            process.StandardInput.WriteLine(moves.Count);
            process.StandardInput.WriteLine(String.Join(" ", startingCoordinate));
            moves.ForEach(m =>
            {
                process.StandardInput.WriteLine(m.Direction + " " + m.Magnitude);
            });
            
            process.StandardInput.WriteLine(Environment.NewLine);

            string output = process.StandardOutput.ReadLine();

            if (!process.WaitForExit(60 * 1000))
            {
                Assert.Fail("Process did not finish after timeout!");
            }

            return ParseResult(output);
        }

        private static FileInfo GetApplicationExecutable()
        {
            string applicationPathFormat = @"..\..\..\RobotCleaner.App\bin\{0}\RobotCleaner.App.exe";
            FileInfo applicationExecutable = new FileInfo(String.Format(applicationPathFormat, "Debug"));

            if (!applicationExecutable.Exists)
                applicationExecutable = new FileInfo(String.Format(applicationPathFormat, "Release"));

            if (!applicationExecutable.Exists)
                Assert.Fail(String.Format("Could not find application executable at {0} or {1}", String.Format(applicationPathFormat, "Debug"), String.Format(applicationPathFormat, "Release")));

            return applicationExecutable;
        }

        private int ParseResult(string output)
        {
            return Int32.Parse(_outputRegex.Match(output).Groups[0].Value);
        }
        
    }
}
