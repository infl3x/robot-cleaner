﻿using RobotCleaner.App.Infrastructure;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App
{
    public interface IOffice
    {
        Grid Initialise(CleanerConfiguration cleanerConfiguration);
        void Clean(int x, int y);
    }

    /// <summary>
    /// Represents the office that the robot will clean
    /// </summary>
    public class Office : IOffice
    {

        private Grid _grid;
        
        public Office()
        {
            _grid = null;
        }

        /// <summary>
        /// Configure the office with the parameters provided
        /// </summary>
        /// <param name="cleanerConfiguration">The office configuration to use</param>
        /// <returns>The office grid created from the parameters provided</returns>
        public Grid Initialise(CleanerConfiguration cleanerConfiguration)
        {
            CheckGridSize(cleanerConfiguration);

            int x = (int)Math.Floor(cleanerConfiguration.OfficeWidth / 2.0);
            int y = (int)Math.Floor(cleanerConfiguration.OfficeLength / 2.0);

            _grid = new Grid()
            {
                LowerXBound = -x,
                UpperXBound = x,
                LowerYBound = -y,
                UpperYBound = y
            };

            return _grid;
        }

        /// <summary>
        /// Check that the width and length of the grid is suitable
        /// </summary>
        /// <param name="cleanerConfiguration">The office configuration to check</param>
        private void CheckGridSize(CleanerConfiguration cleanerConfiguration)
        {
            // Because the required grid must be 100,000 each direction, we need to ensure there's at least 1 more number to represent the (0,0) coordinate.
            // This could be easily changed if we just decrease the even boards by 1 (or increase by 1).
            if ((cleanerConfiguration.OfficeWidth % 2 == 0) || (cleanerConfiguration.OfficeLength % 2 == 0))
                throw new InvalidGridException();
        }

        /// <summary>
        /// Cleans the vertex at the specified coordinate
        /// </summary>
        /// <param name="x">The X coordinate to clean</param>
        /// <param name="y">The Y coordinate to clean</param>
        /// <returns>The cleaned node</returns>
        public void Clean(int x, int y)
        {
            CheckBounds(x, y);
        }

        /// <summary>
        /// Check that the coordinate is not outside the bounds of the office grid
        /// </summary>
        /// <param name="x">The X coordinate to check</param>
        /// <param name="y">The Y coordinate to check</param>
        private void CheckBounds(int x, int y)
        {
            if (x < _grid.LowerXBound || x > _grid.UpperXBound)
                throw new OutOfBoundsException(x, y);
            if(y < _grid.LowerYBound || y > _grid.UpperYBound)
                throw new OutOfBoundsException(x, y);
        }
    }
}
