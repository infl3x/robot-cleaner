﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents the relationship between two movement lines on the grid of the office
    /// </summary>
    public class GridLineRelationship
    {
        /// <summary>
        /// The relationship that the two lines have
        /// </summary>
        public LineRelationship LineRelationship { get; set; }

        /// <summary>
        /// The list of overlapping vertices between two lines on the office grid
        /// </summary>
        public Node[] OverlappingCoordinates { get; set; }

        public GridLineRelationship()
        {
            LineRelationship = LineRelationship.None;
            OverlappingCoordinates = new Node[] { };
        }
    }
}
