﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents a vertex on the office grid
    /// </summary>
    public class Node
    {
        /// <summary>
        /// The X coordinate of the vertex on the office grid
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// The Y coordinate of the vertex on the office grid
        /// </summary>
        public int Y { get; set; }

        public Node(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Node(int[] coordinate)
        {
            X = coordinate[0];
            Y = coordinate[1];
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if(!(obj is Node))
                return false;

            Node other = (Node)obj;
            return (this.X == other.X && this.Y == other.Y);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", X, Y);
        }
    }
}
