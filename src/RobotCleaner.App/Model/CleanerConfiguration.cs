﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents the cleaner configuration model
    /// </summary>
    public class CleanerConfiguration
    {
        /// <summary>
        /// The width of the office board
        /// </summary>
        public int OfficeWidth { get; set; }

        /// <summary>
        /// The length of the office board
        /// </summary>
        public int OfficeLength { get; set; }

        /// <summary>
        /// The starting coordinates of the robot
        /// </summary>
        public int[] StartingCoordinate { get; set; }

        /// <summary>
        /// The list of moves for the robot to take
        /// </summary>
        public List<Vector> Moves { get; set; }

        public CleanerConfiguration()
        {
            OfficeWidth = OfficeLength = 0;

            StartingCoordinate = null;
            Moves = new List<Vector>();
        }
    }
}
