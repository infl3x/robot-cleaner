﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents a movement that the robot will make
    /// </summary>
    public class Vector
    {
        /// <summary>
        /// The direction of the robot's movement vector
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// The magnitude of the robot's movement vector
        /// </summary>
        public int Magnitude { get; set; }
    }
}
