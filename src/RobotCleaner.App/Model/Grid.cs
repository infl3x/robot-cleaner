﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents the grid of the office
    /// </summary>
    public class Grid
    {
        /// <summary>
        /// Smallest X coordinate in the office
        /// </summary>
        public int LowerXBound { get; set; }

        /// <summary>
        /// Largest X coordinate in the office
        /// </summary>
        public int UpperXBound { get; set; }

        /// <summary>
        /// Smallest Y coordinate in the office
        /// </summary>
        public int LowerYBound { get; set; }

        /// <summary>
        /// Largest Y coordinate in the office
        /// </summary>
        public int UpperYBound { get; set; }

        public Grid()
        {
            LowerXBound = UpperXBound = 0;
            LowerYBound = UpperYBound = 0;
        }
    }
}
