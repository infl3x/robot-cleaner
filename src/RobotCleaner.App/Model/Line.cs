﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents a movement line that the robot takes across the grid of the office
    /// </summary>
    public class Line
    {
        /// <summary>
        /// The starting position of the line
        /// </summary>
        public Node Start { get; set; }

        /// <summary>
        /// The finishing position of the line
        /// </summary>
        public Node Finish { get; set; }

        public Line()
        {
            Start = null;
            Finish = null;
        }
    }
}
