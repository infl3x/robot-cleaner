﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Model
{
    /// <summary>
    /// Represents a compass direction to move the robot
    /// </summary>
    public enum Direction : byte {
        E, // East
        W, // West
        S, // South
        N  // North
    }

    /// <summary>
    /// Represents the relationships between two lines of the robot's travel
    /// </summary>
    public enum LineRelationship : int
    {
        None,
        Parallel,
        Collinear,
        Intersection
    }
}
