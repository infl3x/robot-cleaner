﻿using RobotCleaner.App.Services;
using RobotCleaner.App.Infrastructure;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App
{
    public interface IRobot
    {
        void Initialise(int[] startingCoordinates, List<Vector> moves);
        int Clean(IOffice office);
    }

    /// <summary>
    /// Represents the robot that will clean the office
    /// </summary>
    public class Robot : IRobot
    {

        private int _totalCleaned;
        private IOffice _office;
        private IGridService _gridService;
        private int[] _currentCoordinate;
        private List<Vector> _moves;
        private List<Line> _lines;
        private List<Node> _overlappingCoordinates;

        public Robot(IGridService gridService)
        {
            _gridService = gridService;

            _totalCleaned = 1;
            _office = null;
            _currentCoordinate = null;
            _moves = null;
            _lines = new List<Line>();
            _overlappingCoordinates = new List<Node>();
        }

        /// <summary>
        /// Configure the robot with the parameters provided
        /// </summary>
        /// <param name="startingCoordinate">The coordinate which the robot is placed to start</param>
        /// <param name="moves">The list of moves the robot will take</param>
        public void Initialise(int[] startingCoordinate, List<Vector> moves)
        {
            _currentCoordinate = startingCoordinate;
            _moves = moves;
        }

        /// <summary>
        /// Cleans the office parameter provided
        /// </summary>
        /// <param name="office">The office to clean</param>
        /// <returns>The number of vertices that the robot has cleaned</returns>
        public int Clean(IOffice office)
        {
            _office = office;

            ComputeMovementLines();
            RemoveMovementLineIntersections();

            return _totalCleaned - _overlappingCoordinates.GroupBy((c) => new { c.X, c.Y }).Distinct().Count();
        }

        /// <summary>
        /// Figure out the robot's movement lines around the office
        /// </summary>
        private void ComputeMovementLines()
        {
            foreach (Vector vector in _moves)
            {
                _lines.Add(new Line()
                {
                    Start = new Node(_currentCoordinate),
                    Finish = Move(vector)
                });
            }
        }

        /// <summary>
        /// Remove the movement line intersections from the total of cleaned vertices
        /// </summary>
        private void RemoveMovementLineIntersections()
        {
            for (int i = 0; i < _lines.Count; i++)
            {
                if (i == _lines.Count - 1)
                    // Don't try and compare the last line to anything
                    continue;

                for (int j = i + 1; j < _lines.Count; j++)
                {
                    GridLineRelationship gridLineRelationship = _gridService.DoLineSegmentsIntersect(_lines[i], _lines[j]);

                    if (gridLineRelationship.LineRelationship == LineRelationship.Collinear || gridLineRelationship.LineRelationship == LineRelationship.Intersection)
                        ProcessOverlappingCoordinates(_lines[i], _lines[j], gridLineRelationship);
                }
            }
        }

        /// <summary>
        /// Process the relationship between the two movement lines and store any relevant points
        /// </summary>
        private void ProcessOverlappingCoordinates(Line line1, Line line2, GridLineRelationship gridLineRelationship)
        {
            if (gridLineRelationship.OverlappingCoordinates.Length == 0)
                return;

            foreach (Node overlappingCoordinate in gridLineRelationship.OverlappingCoordinates)
            {
                if (overlappingCoordinate.Equals(line2.Start))
                    continue;

                _overlappingCoordinates.Add(overlappingCoordinate);
            }
        }

        /// <summary>
        /// Move the robot along the movement vector provided
        /// </summary>
        /// <param name="vector">The vector for the robot to move along</param>
        private Node Move(Vector vector)
        {
            int netMagnitude = Math.Abs(vector.Magnitude);
            _totalCleaned += netMagnitude;

            switch (vector.Direction)
            {
                case Direction.E:
                    // Move East
                    _currentCoordinate = new int[] { _currentCoordinate[0] + netMagnitude, _currentCoordinate[1] };
                    break;
                case Direction.W:
                    // Move West
                    _currentCoordinate = new int[] { _currentCoordinate[0] - netMagnitude, _currentCoordinate[1] };
                    break;
                case Direction.S:
                    // Move South
                    _currentCoordinate = new int[] { _currentCoordinate[0], _currentCoordinate[1] - netMagnitude };
                    break;
                case Direction.N:
                    // Move North
                    _currentCoordinate = new int[] { _currentCoordinate[0], _currentCoordinate[1] + netMagnitude };
                    break;
                default:
                    throw new ArgumentException("Can't move in direction: " + vector.Direction);
            }

            _office.Clean(_currentCoordinate[0], _currentCoordinate[1]);

            return new Node(_currentCoordinate);
        }

    }
}
