﻿using Ninject;
using RobotCleaner.App.Configuration;
using RobotCleaner.App.Infrastructure;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App
{
    public class Program
    {
        // Set up the dependancy IoC container
        private static readonly IKernel _kernel = new StandardKernel(new RobotCleanerNinjectModule());

        // Cint Robot Cleaning Method:
        // 1. Create office board
        // 2. Clean start position
        // 3. Go through robot's moves:
        //    a. Add the absolute value of the magnitude to the total
        //    b. Compute the movement lines as two points on the grid
        // 4. Examine each of the movement lines against each other:
        //    a. Check if they intersect
        //    b. Store the overlapping points between the two lines
        // 5. Return total of the clean vertices and subtract the number of unique overlapping points

        public static void Main(string[] args)
        {
            // Read the cleaner configuration from the console
            ICleanerConfigurationReader configurationReader = _kernel.Get<CleanerConfigurationReader>();
            CleanerConfiguration officeConfiguration = configurationReader.Read();

            // Create the office model
            IOffice office = _kernel.Get<Office>();
            office.Initialise(officeConfiguration);

            // Create the robot and pass it the office configuration
            IRobot robot = _kernel.Get<Robot>();
            robot.Initialise(officeConfiguration.StartingCoordinate, officeConfiguration.Moves);
            int cleaned = robot.Clean(office);

            // Display the results
            Console.WriteLine("=> Cleaned: " + cleaned);
            Console.ReadLine();
        }
    }
}
