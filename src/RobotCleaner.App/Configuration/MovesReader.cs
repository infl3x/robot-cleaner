﻿using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Configuration
{
    public interface IMovesReader
    {
        List<Vector> Read(int numberOfCommands);
    }

    /// <summary>
    /// Class to read the robot's moves from the user's console input
    /// </summary>
    public class MovesReader : IMovesReader
    {
        private readonly IInputReader _inputReader;

        public MovesReader(IInputReader inputReader)
        {
            _inputReader = inputReader;
        }

        /// <summary>
        /// Read the robot's moves from the user's console input
        /// </summary>
        /// <returns>The moves the robot will make around the office</returns>
        public List<Vector> Read(int numberOfCommands)
        {
            List<Vector> moves = new List<Vector>();

            for (int i = 0; i < numberOfCommands; i++)
            {
                string[] move = _inputReader.ReadLine().Split(' ');

                moves.Add(new Vector()
                {
                    Direction = (Direction)Enum.Parse(typeof(Direction), move[0]),
                    Magnitude = Int32.Parse(move[1])
                });
            }

            return moves;
        }
    }
}
