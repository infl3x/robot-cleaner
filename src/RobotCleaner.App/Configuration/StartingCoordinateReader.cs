﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Configuration
{
    public interface IStartingCoordinateReader
    {
        int[] Read();
    }

    /// <summary>
    /// Class to read the starting coordinate for the robot from the user's console input
    /// </summary>
    public class StartingCoordinateReader : IStartingCoordinateReader
    {
        private readonly IInputReader _inputeReader;

        public StartingCoordinateReader(IInputReader inputReader)
        {
            _inputeReader = inputReader;
        }

        /// <summary>
        /// Read the starting coordinate for the robot from the user's console input
        /// </summary>
        /// <returns>The robot's starting coordinate</returns>
        public int[] Read()
        {
            return _inputeReader.ReadLine().Split(' ').Select(c => Int32.Parse(c)).ToArray();
        }
    }
}
