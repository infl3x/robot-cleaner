﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Configuration
{
    public interface INumberOfCommandsReader
    {
        int Read();
    }

    /// <summary>
    /// Class to read the number of robot moves from the user's console input
    /// </summary>
    public class NumberOfCommandsReader : INumberOfCommandsReader
    {
        private readonly IInputReader _inputeReader;

        public NumberOfCommandsReader(IInputReader inputReader)
        {
            _inputeReader = inputReader;
        }

        /// <summary>
        /// Read the number of robot moves from the user's console input
        /// </summary>
        /// <returns>The number of moves to read</returns>
        public int Read()
        {
            return Int32.Parse(_inputeReader.ReadLine());
        }
    }
}
