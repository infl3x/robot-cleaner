﻿using RobotCleaner.App.Configuration;
using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Configuration
{
    public interface ICleanerConfigurationReader
    {
        CleanerConfiguration Read();
    }

    /// <summary>
    /// Configuration reader which retrieves the commands from the user's console input
    /// </summary>
    public class CleanerConfigurationReader : ICleanerConfigurationReader
    {

        private readonly INumberOfCommandsReader _numberOfCommandsReader;
        private readonly IStartingCoordinateReader _startingCoordinateReader;
        private readonly IMovesReader _movesReader;

        private const string OfficeWidthConfigurationName = "OfficeWidith";
        private const string OfficeLengthConfigurationName = "OfficeLength";

        public CleanerConfigurationReader(INumberOfCommandsReader numberOfCommandsReader, IStartingCoordinateReader startingCoordinateReader, IMovesReader movesReader)
        {
            _numberOfCommandsReader = numberOfCommandsReader;
            _startingCoordinateReader = startingCoordinateReader;
            _movesReader = movesReader;
        }

        /// <summary>
        /// Reads the user's console input
        /// </summary>
        /// <returns>The cleaner configuration model read from the console.</returns>
        public CleanerConfiguration Read()
        {
            int numberOfCommands = _numberOfCommandsReader.Read();

            return new CleanerConfiguration()
            {
                OfficeWidth = ReadOfficeWidth(),
                OfficeLength = ReadOfficeLength(),
                StartingCoordinate = _startingCoordinateReader.Read(),
                Moves = _movesReader.Read(numberOfCommands)
            };
        }

        /// <summary>
        /// Reads the office board's width from the application config
        /// </summary>
        /// <returns>The office width</returns>
        private int ReadOfficeWidth()
        {
            return Int32.Parse(ConfigurationManager.AppSettings[OfficeWidthConfigurationName]);
        }

        /// <summary>
        /// Reads the office board's length from the application config
        /// </summary>
        /// <returns>The office length</returns>
        private int ReadOfficeLength()
        {
            return Int32.Parse(ConfigurationManager.AppSettings[OfficeLengthConfigurationName]);
        }
    }
}
