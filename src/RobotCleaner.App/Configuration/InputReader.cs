﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Configuration
{
    public interface IInputReader
    {
        string ReadLine();
    }

    /// <summary>
    /// Class to abstract the console reading for testability
    /// </summary>
    public class InputReader : IInputReader
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
    
}
