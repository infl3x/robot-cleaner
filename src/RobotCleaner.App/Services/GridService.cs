﻿using RobotCleaner.App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Services
{
    public interface IGridService
    {
        GridLineRelationship DoLineSegmentsIntersect(Line lineA, Line lineB);
    }

    /// <summary>
    /// Grid service class for analysing two of the robot's movement lines for intersecting vertices
    /// </summary>
    public class GridService : IGridService
    {
        /// <summary>
        /// Analyse two of the robot's movement lines to see if they intersect
        /// </summary>
        /// <param name="lineA">The first movement line</param>
        /// <param name="lineB">The second movement lines</param>
        /// <returns>The relationship between the two lines and the amount of overlap shared between them</returns>
        public GridLineRelationship DoLineSegmentsIntersect(Line lineA, Line lineB)
        {
            return DoLineSegmentsIntersect(lineA.Start, lineA.Finish, lineB.Start, lineB.Finish);
        }

        /// <summary>
        /// Overloaded method to analyse two of the robot's movement lines to see if they intersect
        /// </summary>
        private GridLineRelationship DoLineSegmentsIntersect(Node a1, Node a2, Node b1, Node b2)
        {
            Node r = SubtractPoints(a2, a1);
            Node s = SubtractPoints(b2, b1);

            long uNumerator = CrossProduct(SubtractPoints(b1, a1), r);
            long denominator = CrossProduct(r, s);

            if (uNumerator == 0 && denominator == 0)
            {
                // The lines are collinear

                List<Node> touchingVertices = new List<Node>();

                // Do they touch? Are any of the points equal?
                if (EqualPoints(a1, b1))
                    touchingVertices.Add(new Node(a1.X, a1.Y));
                if(EqualPoints(a1, b2))
                    touchingVertices.Add(new Node(a1.X, a1.Y));
                if(EqualPoints(a2, b2))
                    touchingVertices.Add(new Node(a2.X, a2.Y));
                if(EqualPoints(a2, b1))
                    touchingVertices.Add(new Node(a2.X, a2.Y));
                
                // Which way are they aligned?
                bool alignedOnXAxis = IsAlignedOnXAxis(a1, a2, b1, b2);
                bool alignedOnYAxis = IsAlignedOnYAxis(a1, a2, b1, b2);

                int overlap = GetNumberOfCollinearOverlapCoordinates(a1, a2, b1, b2, alignedOnXAxis, alignedOnYAxis);
                if(overlap > 0)
                    return GetGridLineRelationship(LineRelationship.Collinear, GetCollinearOverlappingCoordinates(a1, a2, b1, b2, alignedOnXAxis, alignedOnYAxis));

                if(touchingVertices.Count > 0)
                    return GetGridLineRelationship(LineRelationship.Collinear, touchingVertices.ToArray());

                return GetGridLineRelationship(LineRelationship.Collinear);
            }

            if (denominator == 0)
            {
                // The lines are parallel
                return GetGridLineRelationship(LineRelationship.Parallel);
            }

            double u = (double)uNumerator / (double)denominator;
            double t = CrossProduct(SubtractPoints(b1, a1), s) / (double)denominator;

            if ((t >= 0) && (t <= 1) && (u >= 0) && (u <= 1))
            {
                // The lines intersect
                return GetGridLineRelationship(LineRelationship.Intersection, GetIntersectingCoordinate(a1, a2, b1, b2));
            }

            // No intersection relationship between the two lines
            return GetGridLineRelationship(LineRelationship.None);
        }

        /// <summary>
        /// Check if the two lines are aigned on the Y axis
        /// </summary>
        /// <returns>True if the points are aligned vertically, false otherwise</returns>
        private bool IsAlignedOnYAxis(Node a1, Node a2, Node b1, Node b2)
        {
            return !AllEqual(
                (b1.Y - a1.Y < 0),
                (b1.Y - a2.Y < 0),
                (b2.Y - a1.Y < 0),
                (b2.Y - a2.Y < 0)
            );
        }

        /// <summary>
        /// Check if the two lines are aigned on the X axis
        /// </summary>
        /// <returns>True if the points are aligned horizontally, false otherwise</returns>
        private bool IsAlignedOnXAxis(Node a1, Node a2, Node b1, Node b2)
        {
            return !AllEqual(
                (b1.X - a1.X < 0),
                (b1.X - a2.X < 0),
                (b2.X - a1.X < 0),
                (b2.X - a2.X < 0)
            );
        }

        /// <summary>
        /// Return the list of overlapping vertices between two collinear lines
        /// </summary>
        /// <returns>The list of points overlapping between two collinear lines</returns>
        private Node[] GetCollinearOverlappingCoordinates(Node a1, Node a2, Node b1, Node b2, bool alignedOnXAxis, bool alignedOnYAxis)
        {
            if (alignedOnXAxis)
            {
                // X axis overlap
                int[] orderedX = new int[] { a1.X, a2.X, b1.X, b2.X }.OrderByDescending(x => x).ToArray();
                int xOverlap = GetOrdinalValue(orderedX[1], orderedX[2]);
                List<Node> overlappingCoordinates = new List<Node>();
                for (int x = 0; x < xOverlap; x++)
                {
                    overlappingCoordinates.Add(new Node(orderedX[2] + x, a1.Y));
                }

                return overlappingCoordinates.ToArray();
            }
            else if (alignedOnYAxis)
            {
                // Y axis overlap
                int[] orderedY = new int[] { a1.Y, a2.Y, b1.Y, b2.Y }.OrderByDescending(y => y).ToArray();
                int yOverlap = GetOrdinalValue(orderedY[1], orderedY[2]);
                List<Node> overlappingCoordinates = new List<Node>();
                for (int y = 0; y < yOverlap; y++)
                {
                    overlappingCoordinates.Add(new Node(a1.X, orderedY[2] + y));
                }

                return overlappingCoordinates.ToArray();
            }

            // Collinear, but no overlap
            return new Node[] { };
        }

        /// <summary>
        /// Return the list of overlapping vertices between two intersecting lines
        /// </summary>
        /// <returns>The list of points overlapping between two intersecting lines</returns>
        private Node GetIntersectingCoordinate(Node a1, Node a2, Node b1, Node b2)
        {
            if (a1.Y == a2.Y)
                // Runs horizontal
                return new Node(b1.X, a1.Y);

            // Runs vertical
            return new Node(a1.X, b1.Y);
        }

        /// <summary>
        /// Figure out how much overlap two collinear lines have
        /// </summary>
        /// <returns>The number of overlapping vertices</returns>
        private int GetNumberOfCollinearOverlapCoordinates(Node a1, Node a2, Node b1, Node b2, bool alignedOnXAxis, bool alignedOnYAxis)
        {
            // Do they overlap at all?
            if (alignedOnXAxis)
            {
                // X axis overlap
                int[] orderedX = new int[] { a1.X, a2.X, b1.X, b2.X }.OrderByDescending(x => x).ToArray();
                return GetOrdinalValue(orderedX[1], orderedX[2]);
            }
            else if (alignedOnYAxis)
            {
                // Y axis overlap
                int[] orderedY = new int[] { a1.Y, a2.Y, b1.Y, b2.Y }.OrderByDescending(y => y).ToArray();
                return GetOrdinalValue(orderedY[1], orderedY[2]);
            }

            // Collinear, but no overlap
            return 0;
        }

        /// <summary>
        /// Figure out the absolute ordinal value between two points
        /// </summary>
        /// <returns>The value of the difference between the two points</returns>
        private int GetOrdinalValue(int p1, int p2)
        {
            if ((p1 < 0 && p2 > 0) || (p1 > 0 && p2 < 0))
                // Different signs
                return Math.Abs(p1) + Math.Abs(p2) + 1;
            else if (p1 >= 0 && p2 >= 0)
                // Both greater than 0
                return p1 - p2 + 1;
            else
                // Both less than 0
                return Math.Abs(p2) - Math.Abs(p1) + 1;
        }

        /// <summary>
        /// Helper for returning the line relationship and overlap between two lines
        /// </summary>
        private GridLineRelationship GetGridLineRelationship(LineRelationship lineRelationship, params Node[] overlappingCoordinates)
        {
            return new GridLineRelationship()
            {
                LineRelationship = lineRelationship,
                OverlappingCoordinates = overlappingCoordinates
            };
        }

        /// <summary>
        /// Compute the cross product between two points
        /// </summary>
        private long CrossProduct(Node point1, Node point2)
        {
            return ((long)point1.X * (long)point2.Y) - ((long)point1.Y * (long)point2.X);
        }

        /// <summary>
        /// Subtract the difference between two points
        /// </summary>
        private Node SubtractPoints(Node point1, Node point2)
        {
            return new Node(point1.X - point2.X, point1.Y - point2.Y);
        }

        /// <summary>
        /// Check if both points occupy the same point
        /// </summary>
        /// <returns>True if the points are the same</returns>
        private bool EqualPoints(Node point1, Node point2)
        {
            return (point1.X == point2.X) && (point1.Y == point2.Y);
        }

        /// <summary>
        /// Check if all the boolean values are the same
        /// </summary>
        private bool AllEqual(params bool[] arguments)
        {
            bool firstValue = arguments[0];
            for (int i = 1; i < arguments.Length; i += 1)
            {
                if (arguments[i] != firstValue)
                {
                    return false;
                }
            }
            return true;
        }


    }
}
