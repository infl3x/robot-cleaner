﻿using Ninject.Modules;
using RobotCleaner.App.Configuration;
using RobotCleaner.App.Model;
using RobotCleaner.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Infrastructure
{
    /// <summary>
    /// Dependancy injection module. Defines classes to manage in the IoC container
    /// </summary>
    public class RobotCleanerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IInputReader>().To<InputReader>();
            Bind<INumberOfCommandsReader>().To<NumberOfCommandsReader>();
            Bind<IStartingCoordinateReader>().To<StartingCoordinateReader>();
            Bind<IMovesReader>().To<MovesReader>();
            Bind<ICleanerConfigurationReader>().To<CleanerConfigurationReader>();
            Bind<IRobot>().To<Robot>();
            Bind<IGridService>().To<GridService>();
        }
    }
}
