﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Infrastructure
{
    /// <summary>
    /// Thrown when the robot is moved outside the bounds of the office
    /// </summary>
    public class OutOfBoundsException : Exception
    {
        public OutOfBoundsException(int x, int y) : base(String.Format("Robot coordinates {0},{1} are out of bounds of the office", x, y))
        {
        }
    }
}
