﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCleaner.App.Infrastructure
{
    /// <summary>
    /// Thrown when an invalid grid size is specified
    /// </summary>
    public class InvalidGridException : Exception
    {
        public InvalidGridException() : base("Grid size must be an odd number in order to include 0,0 at the centre")
        {
        }
    }
}
